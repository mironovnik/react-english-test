import {CLICK_ANSWER, ACTIVATE_BUTTON} from "../actions/actionTypes";

const initialState = {
    isButtonEnable: false,
    choosenAnswer: 0
};

export default function lessonReducer(state = initialState, action) {
    switch (action.type) {
        case CLICK_ANSWER: {
            return {
                ...state, choosenAnswer: action.value
            }
        }
        case ACTIVATE_BUTTON: {
            return {
                ...state, isButtonEnable: !action.condition
            }
        }
        default: {
            return state
        }
    }
}