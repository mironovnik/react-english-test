import {CLICK_ANSWER, ACTIVATE_BUTTON} from "./actionTypes";

export function onAnswerClick(answerId, isButtonEnable) {
    return dispatch => {
        dispatch(setChoosenAnswer(answerId));
        if (!isButtonEnable) {
            dispatch(activateButton(isButtonEnable));
        }
    }
}

export function activateButton(isButtonEnable) {
    return {
        type: ACTIVATE_BUTTON,
        condition: isButtonEnable
    }
}

export function setChoosenAnswer(answerId) {
    return {
        type: CLICK_ANSWER,
        value: answerId
    }
}