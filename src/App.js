import React from 'react';
import './App.css';
import Layout from './hoc/Layout/Layout'
import Lesson from './containers/Lesson/Lesson'
import {Route, Switch} from 'react-router-dom'
import LessonCreate from './containers/LessonCreate/LessonCreate'
import LessonList from './containers/LessonList/LessonList'

function App() {
    return (
        <Layout>
            <Switch>
                <Route path={'/create'} component={LessonCreate}/>
                <Route path={'/lesson/:id'} component={Lesson}/>
                <Route path={'/'} component={LessonList}/>
            </Switch>
        </Layout>
    );
}

export default App;
