import React, {Component} from 'react';
import classes from './LessonList.module.scss'
import {NavLink} from 'react-router-dom'

class LessonList extends Component {

    renderLessons() {
        return [1,2,3].map((lesson,index) => {
            return (
                <li key={index}>
                 <NavLink to={`/lesson/${lesson}`}>
                     Урок {lesson}
                 </NavLink>
                </li>
            )
        })
    }

    render() {
        return (
            <div>
                Lessons list
                <ul>
                    {this.renderLessons()}
                </ul>
            </div>
        );
    }
}

export default LessonList;