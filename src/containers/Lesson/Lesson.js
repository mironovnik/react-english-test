import React, {Component} from 'react'
import classes from './Lesson.module.scss'
import ActiveLesson from '../../components/ActiveLesson/ActiveLesson'
import FinishedLesson from '../../components/FinishedLesson/FinishedLesson'
import {connect} from 'react-redux'
import {onAnswerClick} from "../../store/actions/lesson";

class Lesson extends Component {
    state = {
        activeQuestion: 0,
        isButtonEnable: false,
        rightAnswers: [],
        //choosenAnswer: 0,
        lessons: [
            {
                id: 1,
                question: 'Переведите слово:',
                phrase: 'dog',
                rightAnswerId: 1,
                answers: [
                    {id: 1, answer: 'Собака'},
                    {id: 2, answer: 'Кошка'},
                    {id: 3, answer: 'Еда'}
                ]
            },
            {
                id: 2,
                question: 'Вставьте пропуск: ',
                phrase: 'I ... a book',
                rightAnswerId: 2,
                answers: [
                    {id: 1, answer: 'reading'},
                    {id: 2, answer: 'read'},
                    {id: 3, answer: 'having read'}
                ]
            }
        ]
    };

    switchQuestion() {

        const rightAnswers = [...this.state.rightAnswers];
        //if (this.state.choosenAnswer === this.state.lessons[this.state.activeQuestion].rightAnswerId) {
        if (this.props.choosenAnswer === this.state.lessons[this.state.activeQuestion].rightAnswerId) {
            rightAnswers.push('right');
        } else {
            rightAnswers.push('wrong');
        }

        this.setState({
            activeQuestion: this.state.activeQuestion + 1,
            //choosenAnswer: 0,
            rightAnswers
        });

    }

    onAnswerClick(answerId) {
        this.props.onAnswerClick(answerId, this.props.isButtonEnable);
    }

    onFinishedButtonClick() {

        this.setState({
            activeQuestion: 0,
            rightAnswers: [],
            choosenAnswer: 0
        });

    }

    isLessonFinished() {
        return this.state.activeQuestion === this.state.lessons.length;
    }

    componentDidMount() {
        console.log(this.props)
    }

    render() {
        return (
            <div className={classes.Lesson}>
                <div className={classes.LessonWrapper}>
                    <h1>Lesson</h1>
                    {!this.isLessonFinished()
                        ?
                        <ActiveLesson
                            question={this.state.lessons[this.state.activeQuestion].question}
                            phrase = {this.state.lessons[this.state.activeQuestion].phrase}
                            questionNumber={this.state.activeQuestion + 1}
                            totalQuestions={this.state.lessons.length}
                            answers={this.state.lessons[this.state.activeQuestion].answers}
                            onAnswerClick={this.onAnswerClick.bind(this)}
                            onButtonClick={this.switchQuestion.bind(this, null)}
                            isButtonEnable={this.props.isButtonEnable}
                            сhoosenAnswer={this.props.choosenAnswer}
                        />
                        :
                        <FinishedLesson
                            lessons = {this.state.lessons}
                            rightAnswers = {this.state.rightAnswers}
                            totalQuestions={this.state.lessons.length}
                            onFinishedButtonClick = {this.onFinishedButtonClick.bind(this)}
                        />
                    }
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        choosenAnswer: state.lesson.choosenAnswer,
        isButtonEnable: state.lesson.isButtonEnable
    }
}

function mapDispatchToProps(dispatch) {
    return {
        onAnswerClick: (answerId, isButtonEnable) => dispatch(onAnswerClick(answerId, isButtonEnable))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Lesson)

