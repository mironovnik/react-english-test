import React, {Component} from 'react';
import classes from './Auth.module.scss'
import Button from '../../components/UI/Button/Button'
import Input from '../../components/UI/Input/Input'

class Auth extends Component {
    constructor(props) {
        super(props);
    };

    setClasses() {
        const cls = [
            classes.Auth
        ];
        if (this.props.open) {
            cls.push(classes.active)
        }
        return cls.join(' ')
    }

    render() {
        const componentClasses = this.setClasses();
        return (
            <div className={componentClasses}>
                <Input placeholder={'E-mail'} />
                <Input type={'password'} placeholder={'Пароль'}/>
                <Button value={'Войти'} isButtonEnable={true}/>
                <Button value={'Регистрация'} isButtonEnable={true}/>
            </div>
        );
    }
}


export default Auth;