import React from 'react'
import classes from './Button.module.scss'


const Button = props => {
    const cls = [
        classes.Button,
        props.type
    ];
    const defaultValue = 'Click';
    return (
        <button
            className={cls.join(' ')}
            onClick={props.onButtonClick}
            disabled = {!props.isButtonEnable}
        >
            {!!props.value ? props.value : defaultValue}
        </button>
    )
};

export default Button