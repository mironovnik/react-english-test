import  React from 'react'
import classes from './Input.module.scss'

const Input = props => {
    const inputType = props.type || 'text';
    return (
        <div className={classes.Input}>
            <input type={inputType} placeholder={props.placeholder}/>
        </div>
    )
};

export default Input