import React from 'react'
import classes from './Background.module.scss'

const Background = props => {
    const cls = [
        classes.Background
    ];

    if (props.backActive) {
        cls.push(classes.active)
    }

    return (
        <div className={cls.join(' ')}></div>
    )
};

export default Background