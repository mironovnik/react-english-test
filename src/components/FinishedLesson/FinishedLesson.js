import React from 'react'
import classes from './FinishedLesson.module.scss'
import Button from '../../components/UI/Button/Button'

const FinishedLesson = props => {
    let rightAnswers = 0;
    return (
        <div className={classes.FinishedLesson}>
            <h1>Урок окончен</h1>
            <h2>Ваши результаты</h2>
            <ul >
            {
                props.lessons.map((lesson, index) => {
                    if (props.rightAnswers[index] === 'right') {
                        rightAnswers++;
                        return (
                            <li key={lesson.id}>
                                <span>{`${lesson.question}  ${lesson.phrase}  `}</span>
                                <span> - Верно</span>
                            </li>
                        )
                    }
                    return (
                        <li key={lesson.id}>
                            <span>{`${lesson.question}  ${lesson.phrase}  `}</span>
                            <span> - Неверно</span>
                        </li>
                    )
                })
            }
            </ul>
            <p>Правильно {rightAnswers} из {props.lessons.length}</p>
            <Button
                value={'Повторить этот урок'}
                isButtonEnable={true}
                onButtonClick = {props.onFinishedButtonClick}
            />
        </div>
    )
};

export default FinishedLesson