import React from 'react'
import classes from './CardLesson.module.scss'

const CardLesson = props => {
    return (
        <div className={classes.CardLesson}>
            {props.children}
        </div>
    )
};

export default CardLesson