import React from 'react'
import classes from './AnswerItem.module.scss'

const AnswerItem = props => {
    const cls = [
        classes.AnswerItem
    ];

    if (props.сhoosenAnswer > 0 ) {
        cls.push(classes.active);
    }

    return (
        <li
            className={cls.join(' ')}
            onClick={props.onAnswerClick.bind(this, props.answer.id)}
        >
            {props.answer.answer}
        </li>
    )
};

export default AnswerItem