import React from 'react'
import classes from './AnswersList.module.scss'
import AnswerItem from './AnswerItem/AnswerItem'

const AnswersList = props => {
    return (
        <ul className={classes.AnswersList}>
            {
                props.answers.map( answer => {
                    return <AnswerItem
                        key={answer.id}
                        answer={answer}
                        onAnswerClick = {props.onAnswerClick}
                        сhoosenAnswer = {props.сhoosenAnswer === answer.id ? props.сhoosenAnswer : null}
                    />
                })
            }
        </ul>
    )

};

export default AnswersList