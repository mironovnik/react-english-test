import React from 'react'
import classes from './ActiveLesson.module.scss'
import AnswersList from './AnswersList/AnswersList'
import AnswerHead from './AnswerHead/AnswerHead'
import Button from '../../components/UI/Button/Button'

const ActiveLesson = props => {
    return (
        <div className={classes.ActiveLesson}>
            <AnswerHead
                question={props.question}
                phrase = {props.phrase}
                questionNumber={props.questionNumber}
                totalQuestions = {props.totalQuestions}
            />
            <AnswersList
                answers = {props.answers}
                onAnswerClick = {props.onAnswerClick}
                сhoosenAnswer = {props.сhoosenAnswer}
            />
            <Button
                onButtonClick = {props.onButtonClick}
                isButtonEnable = {props.isButtonEnable}
                value={'Проверить'}
            />
        </div>
    )
};

export default ActiveLesson