import React from 'react'
import classes from './AnswerHead.module.scss';

const AnswerHead = props => {
    return (
        <div className={classes.AnswerHead}>
            <div>
                <span> {props.questionNumber}. </span>
                <span>{props.question}</span>
                <p>{props.phrase}</p>
            </div>
            <p> {props.questionNumber} из {props.totalQuestions}</p>
        </div>
    )
};

export default AnswerHead