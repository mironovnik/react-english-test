import React, {Component} from 'react'
import classes from './MainMenu.module.scss'
import {NavLink} from 'react-router-dom'
import Auth from '../../../containers/Auth/Auth'

class MainMenu extends Component {
    constructor(props) {
        super(props);
        this.links = [
            {id: 1, name: 'Главная', href: '/', exact: true},
            {id: 2, name: 'Создание урока', href: '/create', exact: false},
            // {id: 3, name: 'Авторизация', href: '/auth', exact: false}
        ];
    }

    renderLinks(links) {
        return links.map(link => {
            return (
                <li key={link.id}>
                    <NavLink to={link.href} exact={link.exact}>{link.name}</NavLink>
                </li>
            )
        })
    }

    render() {
        return (
            <nav
                className={classes.MainMenu}
                onMouseEnter={this.props.onMenuHover}
                onMouseLeave={this.props.onMenuHover}
            >
                <ul>
                    {this.renderLinks(this.links)}
                    <li>
                        <span onClick={this.props.onAuthClick}>Авторизация</span>
                        <Auth open={this.props.authFormOpen}></Auth>
                    </li>

                </ul>
            </nav>
        )
    }
}


export default MainMenu