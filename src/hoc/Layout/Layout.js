import React, {Component} from 'react'
import classes from './Layout.module.scss'
import MainMenu from '../../components/Navigation/MainMenu/MainMenu'
import Background from '../../components/UI/Background/Background'

class Layout extends Component {
    state = {
        backgroundActive: false,
        authFormOpen: false
    };

    onMenuHover() {
        this.setState({
            backgroundActive: !this.state.backgroundActive
        })
    }

    onAuthClick() {
        this.setState({
            authFormOpen: !this.state.authFormOpen
        })
    }

    onMainClick() {
        if (this.state.authFormOpen) {
            this.setState({
                authFormOpen: !this.state.authFormOpen
            })
        }
    }

    render() {
        return (
            <div className={classes.Layout}>
                <MainMenu
                    onMenuHover={this.onMenuHover.bind(this)}
                    onAuthClick = {this.onAuthClick.bind(this)}
                    authFormOpen = {this.state.authFormOpen}
                />
                <Background backActive={this.state.backgroundActive}/>
                <main onClick={this.onMainClick.bind(this)}>
                    {this.props.children}
                </main>
            </div>
        )
    }
}

export default Layout